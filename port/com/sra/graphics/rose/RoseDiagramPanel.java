/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose;

import java.io.IOException;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

import com.lgc.prodesk.hdr.CatalogedHdrEntry;
import com.lgc.prodesk.mosaicutil.foldmap.FoldMapPrefConst;
import com.lgc.prodesk.seisdata.DataContext;
import com.lgc.prowess.javaseis.smart.SmartFramework;
import com.lgc.prowess.jms.LocationEvent;
import com.lgc.prowess.jms.LocationEventListener;
import com.lgc.prowess.seisdata.SeisUtil;
import com.lgc.prowess.seisdata.SeismicDataset;
import com.sra.util.ASyncDataSetLoader;
import com.sra.util.ReadJSGather;
import edu.mines.jtk.mosaic.ColorBar;
import edu.mines.jtk.mosaic.PlotPanel;
import edu.mines.jtk.util.Check;
import org.javaseis.grid.GridDefinition;

/**
 * RoseDiagram's PlotPanel.
 * @author Steve Angelovich
 */
public class RoseDiagramPanel extends PlotPanel implements LocationEventListener, ASyncDataSetLoader.DataReady {
  private static final long serialVersionUID = 1L;
  private static final PlotPanel.Orientation _orientation = Orientation.X1RIGHT_X2UP;
  private static final Logger LOG = Logger.getLogger(RoseDiagramPanel.class.getName());
  private static final String _user = System.getProperty("user.name");
  private static final int RINGS_DEFAULT = 10;
  private static final int SECTORS_DEFAULT = 36;

  private final String _path;
  private final RoseDiagramView _rd;

  private final ASyncDataSetLoader _loader;
  private final SmartFramework _smartFramework;
  private final String[] _datasetHeaders = new String[3];
  private final String[] _datasetHeaders4D = new String[2];
  private final String[] _datasetHeaders3D = new String[1];
  private int _rings = RINGS_DEFAULT;
  private int _sectors = SECTORS_DEFAULT;
  private boolean _shotPerVolume;

  public RoseDiagramPanel(String path) {
    super(1, 1, _orientation);
    _path = path;

    LOG.fine(String.format("Creating %s with dataset %s", RoseDiagramPanel.class.getSimpleName(), _path));
    _smartFramework = readSmartFramework(_path, _datasetHeaders);
    System.arraycopy(_datasetHeaders,1, _datasetHeaders4D, 0, _datasetHeaders4D.length);
    System.arraycopy(_datasetHeaders4D,1, _datasetHeaders3D, 0, _datasetHeaders3D.length);
    _rd = addRoseDiagram(_rings, _sectors, new float[]{0}, new float[]{0});
    ColorBar cb = addColorBar(_rd);
    cb.setFormat("%f");
    setHFormat("%f");
    setVFormat("%f");
    setHLabel("Offset");
    setVLabel("Offset");
    _loader = new ASyncDataSetLoader(_path, this);
  }

  public String getPath() {
    return _path;
  }

  /**
   * Switch to a new volume.
   *
   * @param hyperKey hypercube to display
   * @param volumeKey volume to display
   */
  public void updateView(final long hyperKey, final long volumeKey, final long frameKey) {
    setTitle("Loading...");
    _loader.switchViewToNewLoc(_shotPerVolume, hyperKey, volumeKey, frameKey);

  }

  public long getFameKey() {
    return _loader.getLastLoadFKey();
  }

  public long getVolumeKey() {
    return _loader.getLastLoadVKey();
  }

  public long getHyperCubeKey() {
    return _loader.getLastLoadHKey();
  }

  public String getHyperCubeHeader() { return _datasetHeaders[0]; }
  public String getVolumeHeader() { return _datasetHeaders[1]; }
  public String getFrameHeader() { return _datasetHeaders[2]; }

  @Override
  public void receiveLocation(LocationEvent locationEvent) {
    LOG.fine(String.format("LocationEvent::receiveEvent user = %s Project = %s AppName = %s; %s", locationEvent.getUser(), locationEvent.getProject(), locationEvent.getAppName(), locationEvent.toString()));

    if (isEventValidForMe(locationEvent)) {

      long[] result = getKeys(locationEvent);
      updateView(result[0],result[1],result[2]);
    }
  }

  public SmartFramework getSmartFramework() {
    return _smartFramework;
  }

  public long[] getKeys(LocationEvent locationEvent) {
    long[] result= new long[3];
    float[] temp;
    int i=0;

    if (is3D()) {
      temp = locationEvent.getLocation(_datasetHeaders3D);
      result[1] = -1l;
      result[0] = -1l;
    } else if(is4D()) {
      temp = locationEvent.getLocation(_datasetHeaders4D);
      result[0] = -1l;
      result[1] = (long) temp[i++];
    } else {
      temp = locationEvent.getLocation(_datasetHeaders);
      result[0] = (long) temp[i++];
      result[1] = (long) temp[i++];
    }

    result[2] = (long)temp[i++];
    return result;
  }

  public boolean isEventValidForMe(LocationEvent locationEvent) {
    final String appName = locationEvent.getAppName();
    final String project = locationEvent.getProject();
    final String username = locationEvent.getUser();
    return appName.equals(FoldMapPrefConst.APP_NAME) && _path.contains(project) && _user.equals(username);
  }

  @Override
  public void onDataReady(final ReadJSGather.GatherData data) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {

        _rd.set(_rings, _sectors, data.recXOrigin, data.recYOrigin);

        if (is3D()) {
          setTitle(String.format("Azimuth fold at %s=%d", _datasetHeaders[2], _loader.getLastLoadFKey()));
        } else if (is4D()) {
          setTitle(String.format("Azimuth fold at %s=%d", _datasetHeaders[1], _loader.getLastLoadVKey()));
        } else {
          setTitle(String.format("Azimuth fold at %s=%d %s=%d", _datasetHeaders[0], _loader.getLastLoadHKey(), _datasetHeaders[1], _loader.getLastLoadVKey()));
        }
      }
    });
  }

  boolean is4D() {
    return getHyperCubeHeader().length() == 0;
  }

  boolean is3D() {
    return getVolumeHeader().length() == 0;
  }

  private static SmartFramework readSmartFramework(final String path, String[] hdrs) {
    Check.argument(hdrs.length == 3, "Need length 2 array for header names");
    SeismicDataset seismicDataset = null;
    try {
      seismicDataset = SeisUtil.openDataset(path);
      final DataContext dataContext = seismicDataset.getDataContext();
      final CatalogedHdrEntry[] frameworkHdrs = dataContext.getFrameworkHdrs();
      hdrs[0] = frameworkHdrs.length <= GridDefinition.HYPERCUBE_INDEX ? "" : frameworkHdrs[GridDefinition.HYPERCUBE_INDEX].getName();
      hdrs[1] = frameworkHdrs.length <= GridDefinition.VOLUME_INDEX ? "" : frameworkHdrs[GridDefinition.VOLUME_INDEX].getName();
      hdrs[2] = frameworkHdrs[GridDefinition.FRAME_INDEX].getName();
      return dataContext.getSmartFramework();
    } catch (IOException e) {
      LOG.severe("Could not read framework headers from dataset");
      throw (new RuntimeException(e));
    } finally {
      try {
        if (seismicDataset != null) {
          seismicDataset.close();
        }
      } catch (IOException e) {
        LOG.severe("Could not read framework headers from dataset");
        throw (new RuntimeException(e));
      }
    }
  }

  private RoseDiagramView addRoseDiagram(int rings, int sectors, float[] x1, float[] x2) {
    RoseDiagramView rdv;
    if (_orientation.equals(Orientation.X1DOWN_X2RIGHT)) {
      rdv = new RoseDiagramView(rings, sectors, x1, x2, RoseDiagramView.Orientation.X1DOWN_X2RIGHT);
    } else if (_orientation.equals(Orientation.X1RIGHT_X2UP)) {
      rdv = new RoseDiagramView(rings, sectors, x1, x2, RoseDiagramView.Orientation.X1RIGHT_X2UP);
    } else {
      throw new IllegalArgumentException();
    }
    addTiledView(0, 0, rdv);
    return rdv;
  }

  public RoseDiagramView getRoseDiagramView() {
    return _rd;
  }

  public void setShotPerVolume(boolean shotPerVolume) {
    if(_shotPerVolume != shotPerVolume) {
      _shotPerVolume = shotPerVolume;
      updateView(getHyperCubeKey(), getVolumeKey(), getFameKey());
    }
  }

  public boolean getShotPerVolume() {
    return _shotPerVolume;
  }

}
