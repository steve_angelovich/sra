/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose;

import edu.mines.jtk.util.ArrayMath;
import edu.mines.jtk.util.Check;


/**
 * Data storage class used by the RoseDiagram for computing hit counts within each ring and segment.
 * 
 * @author Steve Angelovich
 */
public class Rdata {
  private final float _maxOffset ;
  private final int[][] _data ;

  /**
   * Creates an immutable instance of this data storage class
   * @param rings - number of rings created between 0 and max offset
   * @param sectors - number of sectors to divide each ring into
   * @param offset - float array of offsets
   * @param angle - float array of angles in degrees for each offset
   */
  public Rdata(int rings, int sectors, float[] offset, float[] angle) {
    Check.argument(rings > 0,"rings must be greater than 0");
    Check.argument(sectors > 0,"sectors must be greater than 0");
    Check.argument(offset.length == angle.length,"offset.length must equal angle.length");

    if(offset.length == 0) {
      _maxOffset = 0;
    } else {
      _maxOffset = ArrayMath.max(offset);
    }
    _data = new int[sectors][rings];
    
    for(int i=0;i<offset.length;i++) {
      add(offset[i], angle[i]);
    }
  }
 
  /**
   * Returns the maximum offset
   */
  public float getMaxOffset() {
    return _maxOffset ;
  }

  /**
   * Returns the number of rings
   */
  public int getRings() {
    return _data[0].length ;
  }
  
  /**
   * Returns the number of sectors per ring
   */
  public int getSectors() {
    return _data.length ;
  }
  
  /**
   * Returns the distance between rings
   */
  public float getOffsetIncrement() {
    return _maxOffset / getRings() ;
  }
  
  /**
   * Returns the degrees between segments
   */
  public float getSectorIncrement() {
    return 360 / _data.length;
  }
  
  /**
   * Returns the number of hits per segment.
   * @param offset - offset used to lookup ring
   * @param angle - angle used to lookup sector
   */
  public int getHitCount(float offset, float angle) {
    int s = sector(angle) ;
    int r = ring(offset) ;
    return _data[s][r] ;
  }

  /**
   * Returns the number of hits per segment.
   */
  public int getHitCount(int ring, int sector) {
    return _data[sector][ring] ;
  }
  
  /**
   * Returns the maximum hit count for any segment
   */
  public int getMaxHitCount() {
    return ArrayMath.max(_data) ;
  }

  /**
   * Computes offset from x, y 
   */
  public static float[] toOffset(float[] x1, float[] x2) {
    final int len = x1.length;
    Check.argument(len == x2.length ,"Arrays must be the same length");
    float[] offset = new float[len] ;
     for(int i=0;i<len;i++) {
      offset[i] = (float) Math.sqrt(x1[i]*x1[i] + x2[i]*x2[i]) ;
    }
     return offset ;
  }

  /**
   * Computes angle of all x1, x2
   */
  public static float[] toAngle(float[] x1, float[] x2) {
    final int len = x1.length;
    Check.argument(len == x2.length ,"Arrays must be the same length");
    float[] angle = new float[len] ;
     for(int i=0;i<len;i++) {
       angle[i] = (float) toAngle(x1[i], x2[i]);
    }
     return angle ;
  }

  /**
   * Computes angle of x1, x2
   */
  public static double toAngle(double x1, double x2) {
    double result = Math.toDegrees(Math.atan2(x2, x1));
    if (result < 0) {  //always want positive angles
      result += 360;
    }

    return result;
  }

  /**
   * Returns the minimum/starting angle in degrees for the given sector.
   */
  public float getAngle(int sector) {
    return sector * getSectorIncrement();
  }
  
  private int sector(float angle) {
    while(angle < 0)
      angle  = angle  + 360 ;
    while(angle  > 360)
      angle  = angle  - 360 ;
    
    if(angle == 0)
      return 0 ;

   
    float a = 360 / _data.length;
    float ds =  (float) Math.ceil(( angle / a)) ;
    int s = (int) (ds -1) ;
    return s ;
  }

  private int ring(float offset) {
    if(offset < 0 || offset > _maxOffset)
      throw new IllegalArgumentException() ;
    
    if(offset == 0)
      return 0 ;
    
    float o = getOffsetIncrement() ; 
    float dr = (float) Math.ceil(offset / o) ;
    int r = (int) (dr-1) ;

    return r ;
  }
  
  private void add(float offset, float angle) {
    int s = sector(angle) ;
    int r = ring(offset) ;
    _data[s][r]++ ;
  }
   
}