/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;

import java.awt.Graphics2D;

import edu.mines.jtk.mosaic.Transcaler;


/**
 * Picking continuous Offsets.
 * @author Derek Parks
 */
public class PickedOffsets {
  public interface PaintHighlight {
    public void paint(final Graphics2D g2d, final Transcaler ts, final double begin, final double end);
  }

  private final boolean[] _isPicked;
  private final int _n;
  private final double _inc;
  private int _startI;
  private int _endI = 0;
  private final PaintHighlight _paint;

  public PickedOffsets(int n, double inc, PaintHighlight paint) {
    _isPicked = new boolean[n];
    _n = n;
    _inc = inc;
    _startI = n;
    _paint = paint;
  }

  public void pick(final Graphics2D g2d, final Transcaler ts, double value) {

    checkIfIndexPainted(g2d, ts, toIndex(value));
  }

  protected final int toIndex(double value) {
    if(value < 0) {
      value = Math.abs(value);
    }

    int index = (int) Math.floor(value / _inc);
    index = Math.min(index, _n -1);
    return index;
  }

  protected void checkIfIndexPainted(final Graphics2D g2d, final Transcaler ts, final int index) {
    _startI = Math.min(_startI, index);
    _endI = Math.max(_endI, index);
    for(int i= _startI; i <= _endI; i++) {
      recheckIndex(g2d, ts, i);
    }
  }

  protected void recheckIndex(Graphics2D g2d, Transcaler ts, int i) {
    if(!_isPicked[i]) {
      double thisIndex = i * _inc;
      _paint.paint(g2d, ts, thisIndex, (i + 1) * _inc);
      _isPicked[i] = true;
    }
  }

  public double getEndValue() {
     return (_endI+1) * _inc;
  }

  public double getStartValue() {
    return _startI * _inc;
  }

  public boolean isPicked( double value) {
    return _isPicked[toIndex(value)];
  }

  int getEndIndex() {
     return _endI;
  }

  int getStartIndex() {
    return _startI;
  }

  void setEndIndex(int i) {
       _endI = i;
    }

  void setStartIndex(int i) {
      _startI = i;
    }

  int getN() {
      return _n;
    }
}
