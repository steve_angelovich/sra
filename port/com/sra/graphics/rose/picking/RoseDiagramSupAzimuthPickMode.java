/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import edu.mines.jtk.awt.ModeManager;
import edu.mines.jtk.mosaic.Transcaler;

/**
 * 
 * @author Derek Parks
 *
 */
public class RoseDiagramSupAzimuthPickMode extends RoseDiagramAzimuthPickMode{
  private static final long serialVersionUID = 1L;

  public RoseDiagramSupAzimuthPickMode(ModeManager modeManager) {
    super(modeManager,"Select Mirror Azimuths","Select azimuths and their supplementary angle and display in new trace display", KeyEvent.VK_C);
  }

  @Override
  protected PickAzimuths getPicker(final double deltaAZ, final int nSectors, final double deltaOffset, final int rings) {
    return new PickSupAzimuths(nSectors, deltaAZ, new PickedOffsets.PaintHighlight() {
      @Override
      public void paint(Graphics2D g2d, Transcaler ts, double begin, double end) {
        paintHighlightWedge(g2d, ts, deltaOffset * rings, begin, end);
      }
    });
  }
}
