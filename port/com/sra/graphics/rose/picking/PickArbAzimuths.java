/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;

import com.lgc.prodesk.hdr.HdrCatalog;
import com.sra.util.FilteredSeismicDataset;
import com.sra.util.SouRecHeaders;
import edu.mines.jtk.mosaic.Transcaler;

/**
 * Pick arbitrary azimuths.
 * @author Derek Parks
 */
public class PickArbAzimuths extends PickedOffsets implements FilteredSeismicDataset.TraceSelector {
  private SouRecHeaders _headers = null;
  public PickArbAzimuths(int n, double inc, PaintHighlight paint) {
    super(n, inc, paint);
  }

  @Override
  protected void checkIfIndexPainted(final Graphics2D g2d, final Transcaler ts, final int index) {
    recheckIndex(g2d, ts, index);
  }

  @Override
  public boolean includeTrace(int[] hdr) {
    if(_headers == null) return false;

    final double az = _headers.getAZ(hdr);
    return isPicked(az);
  }

  @Override
  public void setHdrs(final HdrCatalog hdrCatalog) {
    _headers = new SouRecHeaders(hdrCatalog);
  }
}
