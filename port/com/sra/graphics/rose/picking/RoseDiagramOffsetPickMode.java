/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;

import com.sra.graphics.rose.RoseDiagramView;
import com.sra.util.FilteredSeismicDataset;
import edu.mines.jtk.awt.ModeManager;
import edu.mines.jtk.mosaic.Transcaler;

/**
 * 
 * @author Derek Parks
 *
 */
public class RoseDiagramOffsetPickMode extends AbstractPickMode {
//  private static final Logger LOG = Logger.getLogger(RoseDiagramOffsetPickMode.class.getName());
  private static final long serialVersionUID = 1L;

  public RoseDiagramOffsetPickMode(final ModeManager modeManager) {
    super(modeManager, "Select Offset", "Select offsets and display in new trace display", KeyEvent.VK_O);
  }


  private PickedOffsets _picked;

  protected void beginPanelSelect(final MouseEvent e, final Transcaler ts) {
    final RoseDiagramView view = getView();
    final double deltaOffset = view.getOffsetIncrement();
    final int rings = view.getNRings();
    _picked = new PickedOffsets(rings, deltaOffset, new PickedOffsets.PaintHighlight() {
      @Override
      public void paint(Graphics2D g2d, Transcaler ts, double begin, double end) {
        paintHighlightCircle(g2d, ts, begin, end);
      }
    });
    _picked.pick((Graphics2D) getTile().getGraphics(), ts, ts.x(getXStart()));
  }

  protected void duringPanelSelect(final MouseEvent e, final Transcaler ts) {
    _picked.pick((Graphics2D) getTile().getGraphics(), ts, ts.x(getXLast()));
  }

  protected void endPanelSelect(final MouseEvent e, final Transcaler ts) {
    final FilteredSeismicDataset.TraceSelector traceSelector =  new FilteredSeismicDataset.OffsetFilter(_picked.getStartValue(), _picked.getEndValue());
    startTraceDisplay(traceSelector);
    _picked = null;
  }

  private static void paintHighlightCircle(final Graphics2D g2d, final Transcaler ts, final double begin, final double end) {

    g2d.setPaint(HIGHLIGHT_COLOR);
    Shape innerCircle = RoseDiagramView.getCircle(begin, ts);
    Shape outerCircle = RoseDiagramView.getCircle(end, ts);
    Area area = new Area(outerCircle);
    area.subtract(new Area(innerCircle));

    g2d.fill(area);
  }
}