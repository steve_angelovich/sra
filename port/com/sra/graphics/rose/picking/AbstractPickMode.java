/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.KeyStroke;

import com.lgc.prodesk.mosaicutil.foldmap.TraceViewerDriver;
import com.lgc.prowess.seisdata.SeismicDataset;
import com.sra.graphics.rose.RoseDiagramPanel;
import com.sra.graphics.rose.RoseDiagramView;
import com.sra.util.FilteredSeismicDataset;
import com.sra.util.TraceViewBuilder;
import edu.mines.jtk.awt.Mode;
import edu.mines.jtk.awt.ModeManager;
import edu.mines.jtk.mosaic.Tile;
import edu.mines.jtk.mosaic.TileAxis;
import edu.mines.jtk.mosaic.Transcaler;

/**
 * Class to handle the common mosaic boilerplate picking code.
 * @author Derek Parks
 */
public abstract class AbstractPickMode extends Mode {
  private static final long serialVersionUID = 1L;

  final static Color HIGHLIGHT_COLOR = new Color(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), Color.YELLOW.getBlue(), 128);

  private static final Logger LOG = Logger.getLogger(AbstractPickMode.class.getName());
  private int _xStart, _yStart, _xLast, _yLast;
  private RoseDiagramView _view;
  private RoseDiagramPanel _panel;
  private Tile _tile;

  public AbstractPickMode(final ModeManager modeManager, final String name, final String desc, final int mk) {
    super(modeManager);
    setName(name);
    setEnabled(true);

    setMnemonicKey(mk);
    setAcceleratorKey(KeyStroke.getKeyStroke(mk, 0));
    setShortDescription(desc);
  }

  private final MouseListener _ml = new MouseAdapter() {
    public void mousePressed(MouseEvent e) {
      beginSelect(e);
    }

    public void mouseReleased(MouseEvent e) {
      endSelect(e);
    }
  };

  private final MouseMotionListener _mml = new MouseMotionAdapter() {
      public void mouseDragged(MouseEvent e) {
        duringSelect(e);
      }
    };

  @Override
  protected void setActive(final Component component, final boolean active) {
    if ((component instanceof Tile) || (component instanceof TileAxis)) {
      if (active) {
        component.addMouseListener(_ml);
      } else {
        component.removeMouseListener(_ml);
      }
    }
  }

  public Tile getTile() {
    return _tile;
  }

  public int getXStart() {
    return _xStart;
  }

  public int getYStart() {
    return _yStart;
  }

  public int getXLast() {
    return _xLast;
  }

  public int getYLast() {
    return _yLast;
  }

  public RoseDiagramView getView() {
    return _view;
  }

  private void beginSelect(MouseEvent e) {
    _xStart = e.getX();
    _yStart = e.getY();

    Object source = e.getSource();
    if (source instanceof Tile && ((Tile) source).getMosaic().getParent() instanceof RoseDiagramPanel) {
      _tile = (Tile)e.getSource();
      _panel = (RoseDiagramPanel) (_tile.getMosaic().getParent());
      _view = _panel.getRoseDiagramView();

      final Transcaler ts = _view.getCombinedTranscaler();

      beginPanelSelect(e, ts);

      ((Tile) source).addMouseMotionListener(_mml);
    }
  }

  private void duringSelect(MouseEvent e) {
    _xLast = e.getX();
    _yLast = e.getY();
    if (_view!=null) {
      final Transcaler ts = _view.getCombinedTranscaler();
      duringPanelSelect(e, ts);
    }
  }

  private void endSelect(MouseEvent e) {
    if (_view != null) {
      _tile.removeMouseMotionListener(_mml);
      final Transcaler ts = _view.getCombinedTranscaler();
      endPanelSelect(e, ts);
    }
    _view = null;
    _panel = null;
    _tile = null;
  }

  public void startTraceDisplay(final FilteredSeismicDataset.TraceSelector ts) {
    TraceViewBuilder builder = new TraceViewBuilder(_panel.getPath(), _panel, 0);
    try {
      _panel.repaint();
      SeismicDataset sd = builder.openDataset();
      ts.setHdrs(sd.getDataContext().getHdrCatalog());
      builder.filter(ts).isShotPerVolume(_panel.getShotPerVolume());

      TraceViewerDriver tvd = builder.build();
      tvd.volumeFrameSelected(_panel.getHyperCubeKey() , _panel.getVolumeKey(), _panel.getFameKey());

    } catch (IOException e) {
      LOG.log(Level.SEVERE, "Error starting trace display", e);
    }
  }

  protected abstract void beginPanelSelect(final MouseEvent e, final Transcaler ts);
  protected abstract void endPanelSelect(final MouseEvent e, final Transcaler ts);
  protected abstract void duringPanelSelect(final MouseEvent e, final Transcaler ts);
}
