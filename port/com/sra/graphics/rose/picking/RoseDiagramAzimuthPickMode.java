/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Area;

import com.sra.graphics.rose.Rdata;
import com.sra.graphics.rose.RoseDiagramView;
import com.sra.util.FilteredSeismicDataset;

import edu.mines.jtk.awt.ModeManager;
import edu.mines.jtk.mosaic.Transcaler;

/**
 * 
 * @author Derek Parks
 *
 */
public class RoseDiagramAzimuthPickMode extends AbstractPickMode {
//  private static final Logger LOG = Logger.getLogger(RoseDiagramAzimuthPickMode.class.getName());
  private static final long serialVersionUID = 1L;
  private PickAzimuths _picked;

  public RoseDiagramAzimuthPickMode(final ModeManager modeManager) {
    super(modeManager,"Select Azimuths","Select azimuths and display in new trace display", KeyEvent.VK_A);
  }

  RoseDiagramAzimuthPickMode(final ModeManager modeManager, final String name, final String desc, final int mk) {
      super(modeManager,name,desc,mk);
  }

  protected PickAzimuths getPicker(final double deltaAZ, final int nSectors, final double deltaOffset, final int rings) {
    return new PickAzimuths(nSectors, deltaAZ, new PickedOffsets.PaintHighlight() {
      @Override
      public void paint(Graphics2D g2d, Transcaler ts, double begin, double end) {
        paintHighlightWedge(g2d, ts, deltaOffset * rings, begin, end);
      }
    });
  }

  protected void beginPanelSelect(final MouseEvent e, final Transcaler ts)  {
    final RoseDiagramView view = getView();
    final double deltaAZ = view.getSectorIncrement();
    final int nSectors = view.getNSectors();
    final double deltaOffset = view.getOffsetIncrement();
    final int rings = view.getNRings();
    _picked = getPicker(deltaAZ, nSectors, deltaOffset, rings);
    _picked.pick((Graphics2D) getTile().getGraphics(), ts, Rdata.toAngle(ts.x(getXStart()), ts.y(getYStart())));

  }

  protected void duringPanelSelect(final MouseEvent e, final Transcaler ts) {
    _picked.pick((Graphics2D) getTile().getGraphics(), ts, Rdata.toAngle(ts.x(getXLast()), ts.y(getYLast())));
  }

  protected void endPanelSelect(final MouseEvent e, final Transcaler ts) {
    final FilteredSeismicDataset.TraceSelector traceSelector = new FilteredSeismicDataset.AzimuthFilter(_picked.getStartValue(), _picked.getEndValue());
    startTraceDisplay(traceSelector);
    _picked = null;
  }

  static void paintHighlightWedge(final Graphics2D g2d, final Transcaler ts, final double maxOffset, final double beginAZ, final double endAZ) {

    g2d.setPaint(HIGHLIGHT_COLOR);
    Area outerCircle = new Area(RoseDiagramView.getCircle(maxOffset, ts));
    Area wedge = new Area(RoseDiagramView.getWedge(maxOffset * 2, beginAZ, endAZ, ts));
    wedge.intersect(outerCircle);

    g2d.fill(wedge);
  }
}