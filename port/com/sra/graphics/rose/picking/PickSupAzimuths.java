/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;

import edu.mines.jtk.mosaic.Transcaler;

/**
 * Picking supplementary azimuths.
 * @author Derek Parks
 *
 */
public class PickSupAzimuths extends PickAzimuths {
  public PickSupAzimuths(int nOffsets, double offsetInc, PaintHighlight paint) {
    super(nOffsets, offsetInc, paint);
  }

  @Override
  protected void recheckIndex(Graphics2D g2d, Transcaler ts, int i) {
    super.recheckIndex(g2d, ts, i);
    final int n = getN();
    int supplementary = n / 2 + i;
    if (supplementary >= n) {
      supplementary -= n;
    }
    super.recheckIndex(g2d, ts, supplementary);
  }

}
