/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

import com.lgc.vel.swing.ViewUtils;
import com.sra.graphics.rose.picking.RoseDiagramArbAzimuthPickMode;
import com.sra.graphics.rose.picking.RoseDiagramAzimuthPickMode;
import com.sra.graphics.rose.picking.RoseDiagramOffsetPickMode;
import com.sra.graphics.rose.picking.RoseDiagramSupAzimuthPickMode;
import edu.mines.jtk.awt.ModeMenuItem;
import edu.mines.jtk.mosaic.PlotFrame;
import edu.mines.jtk.mosaic.TileZoomMode;

/**
 * Toolbar for main menu.
 * @author Derek Parks
 */
public class RoseDiagramMenuToolBar {
  private final PlotFrame _mainFrame;
  private final JFrame _controlFrame;
  public RoseDiagramMenuToolBar(final PlotFrame frame, final JFrame controlFrame,final RoseDiagramPanel panel, final VolumeSelectorPanel vsp) {
    _mainFrame = frame;
    _controlFrame = controlFrame;
    final JMenu fileMenu = new JMenu("File");
    fileMenu.setMnemonic('F');
    fileMenu.add(new ViewUtils.SaveAsPngAction(frame)).setMnemonic('a');
    fileMenu.add(new ShowControls()).setMnemonic('c');

    if(!panel.is3D() ) {
      JCheckBoxMenuItem shotPerVolume = new JCheckBoxMenuItem("Shot per Volume");
      shotPerVolume.setToolTipText("One Shot record is an entire volume of frames. (Default is shot per frame.)");
      shotPerVolume.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          AbstractButton aButton = (AbstractButton) e.getSource();
          final boolean selected = aButton.getModel().isSelected();
          panel.setShotPerVolume(selected);
          vsp.updateSliders();
        }
      });

    fileMenu.add(shotPerVolume).setMnemonic('v');
    fileMenu.add(new CloseAction()).setMnemonic('x');
    }
    final JMenu modeMenu = new JMenu("Mode");
    modeMenu.setMnemonic('M');
    final TileZoomMode zoomMode = frame.getTileZoomMode();
    ModeMenuItem zoomModeMenuItem = new ModeMenuItem(zoomMode);

    modeMenu.add(zoomModeMenuItem);

    RoseDiagramOffsetPickMode offsetPickMode = new RoseDiagramOffsetPickMode(frame.getModeManager());
    ModeMenuItem offsetPickModeMenuItem = new ModeMenuItem(offsetPickMode);
    modeMenu.add(offsetPickModeMenuItem);

    RoseDiagramAzimuthPickMode azPickMode = new RoseDiagramAzimuthPickMode(frame.getModeManager());
    ModeMenuItem azPickModeMenuItem = new ModeMenuItem(azPickMode);
    modeMenu.add(azPickModeMenuItem);

    RoseDiagramArbAzimuthPickMode arbPickMode = new RoseDiagramArbAzimuthPickMode(frame.getModeManager());
    ModeMenuItem arbPickModeMenuItem = new ModeMenuItem(arbPickMode);
    modeMenu.add(arbPickModeMenuItem);

    RoseDiagramSupAzimuthPickMode supPickMode = new RoseDiagramSupAzimuthPickMode(frame.getModeManager());
    ModeMenuItem supPickModeMenuItem = new ModeMenuItem(supPickMode);
    modeMenu.add(supPickModeMenuItem);

    final JMenuBar menuBar = new JMenuBar();
    menuBar.add(fileMenu);
    menuBar.add(modeMenu);
    frame.setJMenuBar(menuBar);

    zoomModeMenuItem.doClick();
    zoomMode.setActive(true);
  }

  private final class CloseAction extends AbstractAction {
    private static final long serialVersionUID = 1L;

    private CloseAction() {
      super("Exit");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      _mainFrame.dispose();
      _controlFrame.dispose();
    }
  }

  private class ShowControls extends AbstractAction {
    private static final long serialVersionUID = 1L;

    private ShowControls() {
      super("Show Controls");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       _controlFrame.setVisible(true);
    }
  }
}
