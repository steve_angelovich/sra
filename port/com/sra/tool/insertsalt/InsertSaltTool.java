/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.tool.insertsalt;

import java.util.logging.Logger;

import com.lgc.prodesk.seisdata.DataCapsuleContext;
import com.lgc.prowess.exec.JobContext;
import com.lgc.prowess.seisdata.DataCapsule;
import com.lgc.prowess.seisdata.SeisData;
import com.lgc.prowess.tool.InitPhaseException;
import com.lgc.prowess.tool.ThreadTool;
import com.lgc.prowess.tool.ToolContext;

import edu.mines.jtk.util.Almost;

public class InsertSaltTool extends ThreadTool {
  private static final Logger LOG = Logger.getLogger(InsertSaltTool.class.getName()) ;

  //keep an array of the names of the items in the capsule
  //to make it easy to access them
  private String[] _names;

  @Override
  public void initCapsule(JobContext jobContext, ToolContext toolContext,
      DataCapsuleContext dataCapsuleContext) throws InitPhaseException {

    //there will be one DataConext for each dataset read with a JDI or JDM.
    LOG.info("Number of items in input capsule = "  + dataCapsuleContext.size()) ;
    _names = dataCapsuleContext.getNames();
    if(_names.length != 2) 
      throw new InitPhaseException("This tool requires 2 data contexts in the capsule") ;

    //we only intend to output the default, remove the others
    for(int i=1;i<_names.length;i++) 
      dataCapsuleContext.remove(_names[i]) ;
    
    LOG.info("Number of items in output capsule = "  + dataCapsuleContext.size()) ;
  }
  
  @Override
  public void run() {
    DataCapsule capsule = null ; 
    
    while((capsule=super.getDataCapsule()) != null) {
      
      SeisData[] data = capsule.getAll() ;
      
      float[][] vel = data[0].getTraces() ;
      float[][] salt = data[1].getTraces() ;
      traceSub(vel, salt) ;
    
      for(int i=1;i<data.length;i++) {
        SeisData sd = capsule.remove(_names[i]);
        sd.free() ;
      }
      
      super.putDataCapsule(capsule) ;
    }
  }
  
  private void traceSub(float[][] vel, float[][] salt) {
    int count = vel.length;
    int samples = vel[0].length;

    for (int i = 0; i < count; i++) {
      float[] vel1 = vel[i];
      float[] salt1 = salt[i];
      for (int j = 0; j < samples; j++) {
        if(!Almost.FLOAT.equal(salt1[j], 0f)) {
        	vel1[j] = salt1[j] ;
        }
      }
    }
  }

  @Override
  public void abort() {
  }

  @Override
  public void completeNormally() {
  }
}
