/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.tool.isolatesalt;

import com.lgc.prodesk.flowbuilder.pui.Parm;
import com.lgc.prodesk.flowbuilder.pui.pw.FloatParm;
import com.lgc.prodesk.flowbuilder.pui.pw.PWParm;
import com.lgc.prodesk.flowbuilder.pui.pw.PWProc;

public class IsolateSaltProc extends PWProc {
	static final String MINVEL = "MINVEL" ;
	static final String MAXVEL = "MAXVEL" ;

	private PWParm minVelParm = new FloatParm(this, MINVEL, "Minimum salt velocity", "Minimum velocity to be considered salt", 0) ;
	private PWParm maxVelParm = new FloatParm(this, MAXVEL, "Maximum salt velocity", "Maximum velocity to be considered salt", 0) ;
	private final Parm[] _contents = new Parm[] {minVelParm, maxVelParm} ;
	
	@Override
	public Parm[] getMenuParms() {
		return _contents ;
	}

}
