/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.tool.isolatesalt;

import java.util.Arrays;

import com.lgc.gpr.util.ParameterSet;
import com.lgc.prodesk.seisdata.DataContext;
import com.lgc.prowess.exec.JobContext;
import com.lgc.prowess.seisdata.SeisData;
import com.lgc.prowess.tool.InitPhaseException;
import com.lgc.prowess.tool.SimpleTool;
import com.lgc.prowess.tool.ToolContext;

public class IsolateSaltTool extends SimpleTool {

	private float _minSalt;
	private float _maxSalt;

	@Override
	public DataContext init(JobContext jobContext, ToolContext toolContext,
			DataContext dataContext) throws InitPhaseException {
		ParameterSet menuParms = toolContext.getMenuParms() ;
		 _minSalt = menuParms.getFloat(IsolateSaltProc.MINVEL, -1) ;
		 if(_minSalt < 0) {
			 throw new InitPhaseException("Salt minimum velocity is not valid") ;
		 }
		 _maxSalt = menuParms.getFloat(IsolateSaltProc.MAXVEL, -1) ;
		 if(_maxSalt < 0) {
			 throw new InitPhaseException("Salt maximum velocity is not valid") ;
		 }
		 
		 
		return dataContext ;
	}

	@Override
	public SeisData exec(SeisData data) {
		
		int count = data.countTraces() ;
		for(int i=0;i<count;i++) {
			float[] in = data.getTrace(i) ;
			float[] out = new float[in.length] ;
			Arrays.fill(out, 0f) ;
			boolean in_salt = false ;
			for(int j=0;j<in.length;j++) {
				if(in_salt == true) {
					if(in[j] < _minSalt || in[j] > _maxSalt)
						break ;
					out[j] = in[j] ;
				} else if(in[j] >= _minSalt) {
					in_salt = true ;
					out[j] = in[j] ;
				}
			}
			System.arraycopy(out, 0, in, 0, in.length) ;
		}
		return data;
	}
	@Override
	public void abort() {
	}

	@Override
	public void completeNormally() {
	}

}
