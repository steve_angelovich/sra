/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.util;

import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.lgc.prodesk.hdr.CatalogedHdrEntry;
import com.lgc.prodesk.hdr.HdrCatalog;
import com.lgc.prodesk.hdr.HdrEntry;
import com.lgc.prodesk.seisdata.DataContext;
import com.lgc.prowess.javaseis.smart.SmartFramework;
import com.lgc.prowess.seisdata.SeisData;
import com.lgc.prowess.seisdata.SeisUtil;
import com.lgc.prowess.seisdata.SeismicDataset;

import org.javaseis.grid.GridDefinition;

/**
 * Read a JavaSeis dataset from disk.
 */
public class ReadJSGather implements AutoCloseable {
  private static final Logger LOG = Logger.getLogger(ReadJSGather.class.getName());

  /**
   * Small container class for holding the source/receiver vectors.
   */
  public class GatherData {
    public final int count;
    public final float[] cdpX;
    public final float[] cdpY;

    public final float[] recXOrigin;
    public final float[] recYOrigin;

    /**
     * Private constructor on purpose.
     *
     * @param count number of traces read.
     */
    private GatherData(final int count) {
      this.count = count;

      /**
       * CDP_X
       */
      cdpX = new float[count];

      /**
       * CDP_Y
       */
      cdpY = new float[count];

      /**
       * X distance from (0,0)
       */
      recXOrigin = new float[count];
      /**
       * Y distance from (0,0)
       */
      recYOrigin = new float[count];
    }
  }

  private final SeismicDataset _seismicDataset;
  private final CatalogedHdrEntry _checdpX;
  private final CatalogedHdrEntry _checdpY;
  private final CatalogedHdrEntry _chesouX;
  private final CatalogedHdrEntry _chesouY;
  private final CatalogedHdrEntry _cherecX;
  private final CatalogedHdrEntry _cherecY;
  private final DataContext _dataContext;
  private final int[][] _map;
  private final long _minVolume;
  private final long _minFrame;
  private final long _incFrame;
  private final long _incVolume;
  private final long _nFrames;


  /**
   * Open and read a JS dataset at the given path.
   *
   * @param path path to the dataset
   * @throws IOException on disk error
   */
  public ReadJSGather(final String path) throws IOException {
    this(SeisUtil.openDataset(path));
  }

  /**
   * Constructor of a given seismic dataset.
   *
   * @param seismicDataset a seismic dataset
   * @throws IOException on disk error
   */
  public ReadJSGather(final SeismicDataset seismicDataset) throws IOException {
    _seismicDataset = seismicDataset;
    _dataContext = _seismicDataset.getDataContext();

    final SmartFramework smartFramework = _dataContext.getSmartFramework();
    _minFrame = smartFramework.getMinFrame();
    _incFrame = smartFramework.getFrameInc();
    _incVolume = smartFramework.getVolumeInc();
    _nFrames = smartFramework.getFramesPerVolume();

    _minVolume = smartFramework.getMinVolume();

    final long[] axisLengths = smartFramework.getAxisLengths();
    if(axisLengths.length > GridDefinition.VOLUME_INDEX) {
      _map = new int[(int) axisLengths[GridDefinition.VOLUME_INDEX]][(int) axisLengths[GridDefinition.FRAME_INDEX]];
    } else {
      _map = null;
    }

    final HdrCatalog hdrCatalog = _dataContext.getHdrCatalog();
    _checdpX = hdrCatalog.getEntry(HdrEntry.CDP_X);
    _checdpY = hdrCatalog.getEntry(HdrEntry.CDP_Y);

    _chesouX = hdrCatalog.getEntry(HdrEntry.SOU_X);
    _chesouY = hdrCatalog.getEntry(HdrEntry.SOU_Y);

    _cherecX = hdrCatalog.getEntry(HdrEntry.REC_X);
    _cherecY = hdrCatalog.getEntry(HdrEntry.REC_Y);
  }

  /**
   * Read one entire volume from the seismic dataset.
   *
   * @param hKey hypercube logical key
   * @param vKey volume logical key
   * @return The source/receiver vectors and CDP locations
   * @throws IOException on disk error
   */
  public GatherData readVolume(final long hKey, final long vKey) throws IOException {
    _seismicDataset.readTraceMap(_map, hKey);
    final long volumeIndex = (vKey/_incVolume) - _minVolume;

    int totalTraces = 0;
    for (long i = 0; i < _nFrames; i++) {
      totalTraces += _map[(int) volumeIndex][(int) i];
    }

    GatherData result = new GatherData(totalTraces);
    int pos = 0;
    for (long i = 0; i < _nFrames; i++) {
      final int nTraces = _map[(int) volumeIndex][(int) i];
      if (nTraces != 0) {
        SeisData data = null;
        try {
          data = _seismicDataset.readFrame(hKey, vKey, (i *_incFrame) + _minFrame);

          pos = readFrameHeaders(result, pos, nTraces, data);
        } catch (ClosedByInterruptException ce) {
          LOG.log(Level.FINE, "Ignored ClosedByInterruptException. Got Interrupt to read another part of the data.", ce);
        } finally {
          if (data != null) {
            data.free();
          }
        }

      }
    }

    return result;
  }

  private int readFrameHeaders(GatherData result, int pos, int nTraces, SeisData data) {
    for (int j = 0; j < nTraces; j++) {
      int[] hdr = data.getHdr(j);
      result.cdpX[pos] = _checdpX.getFloatVal(hdr);
      result.cdpY[pos] = _checdpY.getFloatVal(hdr);

      result.recXOrigin[pos] = _cherecX.getFloatVal(hdr) - _chesouX.getFloatVal(hdr);
      result.recYOrigin[pos] = _cherecY.getFloatVal(hdr) - _chesouY.getFloatVal(hdr);
      pos++;
    }
    return pos;
  }

  /**
   * Read one frame from the seismic dataset.
   *
   * @param hKey hypercube logical key
   * @param vKey volume logical key
   * @param fKey frame logical key
   * @return The source/receiver vectors and CDP locations
   * @throws IOException on disk error
   */
  public GatherData readGather(final long hKey, final long vKey, final long fKey) throws IOException {
    LOG.fine(String.format("readingFrame=%d", fKey));

    SeisData data = null;
    try {
      data = _seismicDataset.readFrame(hKey, vKey, fKey);
      if(data == null) {
         return new GatherData(0);
      }

      GatherData result = new GatherData(data.countTraces());
      readFrameHeaders(result, 0, result.count, data);
      return result;
    } finally {
      if (data != null) {
        data.free();
      }
    }
  }

  /**
   * Close the Seismic dataset.
   */
  public void close() throws IOException {
    _seismicDataset.close();
  }

}
