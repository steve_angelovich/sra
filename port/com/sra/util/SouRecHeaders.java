/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.util;


import com.lgc.prodesk.hdr.CatalogedHdrEntry;
import com.lgc.prodesk.hdr.HdrCatalog;
import com.lgc.prodesk.hdr.HdrEntry;
import com.sra.graphics.rose.Rdata;

/**
 * Struct for holding source receiver positions.
 * @author Derek Parks`
 *
 */
public class SouRecHeaders {
  private final CatalogedHdrEntry _souX;
  private final CatalogedHdrEntry _souY;
  private final CatalogedHdrEntry _recX;
  private final CatalogedHdrEntry _recY;

  public SouRecHeaders(final HdrCatalog hdrCatalog) {
    _souX = hdrCatalog.getEntry(HdrEntry.SOU_X);
    _souY = hdrCatalog.getEntry(HdrEntry.SOU_Y);

    _recX = hdrCatalog.getEntry(HdrEntry.REC_X);
    _recY = hdrCatalog.getEntry(HdrEntry.REC_Y);
  }

  public double getAZ(int[] hdr) {
    final double x = _recX.getFloatVal(hdr) - _souX.getFloatVal(hdr);
    final double y = _recY.getFloatVal(hdr) - _souY.getFloatVal(hdr);


    return Rdata.toAngle(x, y);
  }

  public double getOffset(int[] hdr) {
    final double x = _recX.getFloatVal(hdr) - _souX.getFloatVal(hdr);
    final double y = _recY.getFloatVal(hdr) - _souY.getFloatVal(hdr);

    return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
  }

}
