package com.sra.util;


import java.io.IOException;
import java.nio.channels.ClosedByInterruptException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.lgc.prodesk.hdr.HdrEntry;
import com.lgc.prodesk.seisdata.DataContext;
import com.lgc.prowess.seisdata.DataContextModifier;
import com.lgc.prowess.seisdata.SeisData;
import com.lgc.prowess.seisdata.SeismicDataset;
import org.javaseis.properties.DataDomain;
import org.javaseis.properties.Units;

/**
 * A {@link FilteredSeismicDataset} that flattens the volume dimension into one huge frame.
 */
public class FlattenedFilteredSeismicDataset extends FilteredSeismicDataset {
  private static final Logger LOG = Logger.getLogger(FlattenedFilteredSeismicDataset.class.getName());

  public FlattenedFilteredSeismicDataset(SeismicDataset ds, TraceSelector ts) throws IOException {
    super(ds, ts);
  }

  @Override
  protected DataContext buildDataContext() throws IOException {

    final DataContext  inDC = getDCIn();
    DataContextModifier dcm = new DataContextModifier(inDC);

    dcm.removeSeisGridAxis(2);

    final int newMaxTrace = (int) (getNTracesIn() * getNFramesIn());

    dcm.setSeisGridAxis(newMaxTrace, 0., 1., 0, 1, DataDomain.UNKNOWN, Units.UNKNOWN, HdrEntry.SEQNO, 1);

    return dcm.getDataContext();
  }

  @Override
  public SeisData readFrame(long hypercubeKey, long volumeKey, long frameKey) throws IOException {
    SeisData result = copyInFramesToOutFrame(hypercubeKey, volumeKey);
    nullNonIncluded(result);
    return result;
  }


  private SeisData copyInFramesToOutFrame(long hypercubeKey, long volumeKey) throws IOException {
    updateTraceMap(hypercubeKey);
      final long volumeIndex = volumeKey - getMinVolumeIn();
      SeisData result = getNewSD();

      int traceOutI = 0;
      for (long i = 0; i < getNFramesIn(); i += getIncFrameIn()) {

        final int nTraces = getTraceMap((int) volumeIndex,(int) i);
        if (nTraces != 0) {
          SeisData data = null;
          try {
            data = getDS().readFrame(hypercubeKey, volumeKey, (i * getIncFrameIn()) + getMinFrameIn());
            final float[][] traceBuffer = data.getTraceBuffer();
            final int[][] hdrBuffer = data.getHdrBuffer();
            for (int j = 0; j < traceBuffer.length; j++) {
              System.arraycopy(hdrBuffer[j], 0, result.getHdr(traceOutI), 0, hdrBuffer[j].length);
              System.arraycopy(traceBuffer[j], 0, result.getTrace(traceOutI), 0, traceBuffer[j].length);
              getSeqno().setIntVal(result.getHdr(traceOutI), traceOutI);
              traceOutI++;
            }
          } catch (ClosedByInterruptException ce) {
            LOG.log(Level.FINE, "Ignored ClosedByInterruptException. Got Interrupt to read another part of the data.", ce);
          } finally {
            if (data != null) {
              data.free();
            }
          }

        } else {
          for (int j = 0; j < getNTracesIn(); j++) {
            result.nullTrace(traceOutI);
            getSeqno().setIntVal(result.getHdr(traceOutI), traceOutI);
            traceOutI++;
          }
        }
      }
      return result;
    }

}
