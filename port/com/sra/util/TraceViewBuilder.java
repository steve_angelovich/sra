/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.util;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.lgc.prodesk.mosaicutil.foldmap.TraceViewerDriver;
import com.lgc.prowess.display.apps.ViewerExitListener;
import com.lgc.prowess.proj.DbLine;
import com.lgc.prowess.proj.DbUtils;
import com.lgc.prowess.seisdata.SeisUtil;
import com.lgc.prowess.seisdata.SeismicDataset;
import com.lgc.prowess.swing.SwingUtils;

/**
 * Builder style class for popping up a TraceView.
 * @author Derek Parks
 */
public class TraceViewBuilder {
  private static final Logger LOG = Logger.getLogger(TraceViewBuilder.class.getName());
  private final int _ensemblesPerScreen = 1;
  private final JFrame _frame;
  private final String _path;
  private SeismicDataset _dataSet;
  private DbLine _dbLine;
  private FilteredSeismicDataset.TraceSelector _traceSelector;
  private boolean _isShotPerVolume = false;

  public TraceViewBuilder(final String path, final Component c, final int hyperCube) {
    _path = path;
    _frame = (JFrame) SwingUtilities.getWindowAncestor(c);
  }


  public TraceViewBuilder filter(FilteredSeismicDataset.TraceSelector traceSelector) {
    _traceSelector =  traceSelector;
    return this;
  }

  public TraceViewBuilder isShotPerVolume(final boolean isShotPerVolume) {
    _isShotPerVolume = isShotPerVolume;
    return this;
  }

  public SeismicDataset openDataset() throws IOException {
    _dbLine = DbUtils.createDbLine((new File(_path)).getParent());
    SeismicDataset dataset = SeisUtil.openDataset(_path);
    _dataSet = dataset;
    return _dataSet;
  }

  public TraceViewerDriver build() throws IOException {
    Dimension dim = _frame.getSize();
    Point loc = _frame.getLocation();
    loc.x = loc.x + dim.width;

    final SeismicDataset ds;
    if(_isShotPerVolume) {
      ds = new FlattenedFilteredSeismicDataset(_dataSet, _traceSelector);
    } else {
      ds = new FilteredSeismicDataset(_dataSet, _traceSelector);
    }

    String[] locationLabels = new String[]{"Offset", "Offset"};
    TraceViewerDriver driver = new TraceViewerDriver(ds, _dbLine, dim, loc, locationLabels, _ensemblesPerScreen, false);

    driver.addViewerExitListener(new ViewerExitListener() {
      @Override
      public void viewerExit(boolean stopFlow) {
        try {
          ds.close();
        } catch (IOException e) {
          LOG.log(Level.SEVERE, "Error closing dataset", e);
        }
      }
    });

    driver.setTitle(((ds.getDescName() != null) ? ds.getDescName() : _path));
    SwingUtils.positionDlgToWindow(_frame, driver.getFrame());
    driver.getFrame().setVisible(true);
    return driver;
    }
}
