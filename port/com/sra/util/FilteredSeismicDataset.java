package com.sra.util;

import java.io.IOException;
import java.util.logging.Logger;

import com.lgc.prodesk.hdr.CatalogedHdrEntry;
import com.lgc.prodesk.hdr.HdrCatalog;
import com.lgc.prodesk.hdr.HdrEntry;
import com.lgc.prodesk.seisdata.DataContext;
import com.lgc.prowess.javaseis.smart.SmartFramework;
import com.lgc.prowess.seisdata.DataCapsule;
import com.lgc.prowess.seisdata.History;
import com.lgc.prowess.seisdata.SeisData;
import com.lgc.prowess.seisdata.SeismicDataset;
import org.javaseis.grid.GridDefinition;

/**
 * Class for wrapping an input {@link SeismicDataset} and filtering out some traces based on a given {@link TraceSelector}.
 * Only tested for use with {@link com.lgc.prodesk.mosaicutil.foldmap.TraceViewerDriver}.
 */
public class FilteredSeismicDataset implements SeismicDataset {
  private static final Logger LOG = Logger.getLogger(FilteredSeismicDataset.class.getName());
  public interface TraceSelector {
    public boolean includeTrace(int[] hdr);
    public void setHdrs(final HdrCatalog hdrCatalog);
  }

  private final SeismicDataset _ds;
  private final DataContext _dcIn;
  private final DataContext _dcOut;
  private final TraceSelector _ts;

  private final long _minVolumeIn;

  private final long _minFrameIn;
  private final long _incFrameIn;
  private final long _nFramesIn;
  private final long _nTracesIn;
  private final int[][] _map;

  private final CatalogedHdrEntry _seqno;

  public FilteredSeismicDataset(SeismicDataset ds, TraceSelector ts) throws IOException {
    _ds = ds;
    _dcIn = _ds.getDataContext(); //
    _ts = ts;

    SmartFramework inSF = _dcIn.getSmartFramework();
    _minFrameIn = inSF.getMinFrame();
    _incFrameIn = inSF.getFrameInc();
    _nFramesIn = inSF.getFramesPerVolume();
    _minVolumeIn = inSF.getMinVolume();
    _nTracesIn = inSF.getMaxTracesPerFrame();
    final long[] axisLengths = inSF.getAxisLengths();

    if(axisLengths.length == 3) {
      _map = new int[1][(int) axisLengths[GridDefinition.FRAME_INDEX]];
    } else {
      _map = new int[(int) axisLengths[GridDefinition.VOLUME_INDEX]][(int) axisLengths[GridDefinition.FRAME_INDEX]];
    }

    _seqno =  _dcIn.getHdrCatalog().getEntry(HdrEntry.SEQNO);
    if(_seqno == null) {
      throw  new IllegalArgumentException(String.format("Input dataset doesn't contain standard header %s",HdrEntry.SEQNO.getName()));
    }
    _dcOut = buildDataContext();
  }

  protected DataContext buildDataContext() throws IOException {
    return getDCIn();
  }

  protected DataContext getDCIn() {
    return _dcIn;
  }

  protected long getNTracesIn() {
    return _nTracesIn;
  }

  protected long getNFramesIn() {
    return _nFramesIn;
  }


  protected long getMinVolumeIn() {
    return _minVolumeIn;
  }

  protected long getMinFrameIn() {
    return _minFrameIn;
  }

  protected long getIncFrameIn() {
    return _incFrameIn;
  }

  protected TraceSelector getTS() {
      return _ts;
  }

  protected SeismicDataset getDS() {
      return _ds;
  }

  protected int getTraceMap(int i, int j) {
    return _map[i][j];
  }

  protected void updateTraceMap(long hypercubeKey) throws IOException {
    getDS().readTraceMap(_map, hypercubeKey);
  }

  protected CatalogedHdrEntry getSeqno(){
    return _seqno;
  }

  protected SeisData getNewSD() {
    return SeisData.allocEnsemble(_dcOut);
  }

  public static class OffsetFilter implements TraceSelector {
    private final double _minOffset, _maxOffset;
    private SouRecHeaders _headers = null;

    public OffsetFilter(final double maxOffset) {this(0, maxOffset);}

    public OffsetFilter(final double minOffset, final double maxOffset) {
      _maxOffset = maxOffset;
      _minOffset = minOffset;
    }

    @Override
    public void setHdrs(final HdrCatalog hdrCatalog) {
      _headers = new SouRecHeaders(hdrCatalog);
    }

    @Override
    public boolean includeTrace(int[] hdr) {

      if(_headers == null) return false;
      final double offset = _headers.getOffset(hdr);

      return offset >= _minOffset && offset <= _maxOffset;
    }
  }

  public static class AzimuthFilter implements TraceSelector {
    private final double _startAZ, _endAZ;
    private SouRecHeaders _headers = null;


    public AzimuthFilter(final double startAZ, final double endAZ) {
      _endAZ = endAZ;
      _startAZ = startAZ;
    }

    @Override
    public void setHdrs(final HdrCatalog hdrCatalog) {
      _headers = new SouRecHeaders(hdrCatalog);
    }

    @Override
    public boolean includeTrace(int[] hdr) {
      if(_headers == null) return false;

      final double az = _headers.getAZ(hdr);

      if(_endAZ > _startAZ) {
        return (az <= _startAZ && az >= 0) ||  (az >= _endAZ && az <= 360);
      } else {
        return az <= _startAZ && az >= _endAZ;
      }
    }
  }

  /**
   * Return a new datacontext that represent the new 3D view of the dataset.
   */
  @Override
  public DataContext getDataContext() throws IOException {
    return _dcOut;
  }

  @Override
  public SeisData readFrame(long hypercubeKey, long volumeKey, long frameKey) throws IOException {
    SeisData result = _ds.readFrame(hypercubeKey,volumeKey,frameKey);
    nullNonIncluded(result);
    return result;
  }

  protected void nullNonIncluded(SeisData result) {

    int[][] hdrBuffer = result.getHdrBuffer();

    for(int i=0; i < hdrBuffer.length; i++) {

      if (!result.isNull(i) && !_ts.includeTrace(hdrBuffer[i])) {
        result.nullTrace(i);
      }
    }
  }

  @Override
  public SeisData readFrame(long hypercubeKey, long volumeKey, long frameKey, long minTrace, long maxTrace) throws IOException {
    return _ds.readFrame(hypercubeKey, volumeKey, frameKey, minTrace, maxTrace);
  }


  @Override
  public long traceCount() throws IOException {
    return _ds.traceCount();
  }

  @Override
  public void readTrace(long hypercubeKey, long volumeKey, long frameKey, long traceKey, float[] trace, int[] hdr) throws IOException {
    _ds.readTrace(hypercubeKey, volumeKey, frameKey, traceKey, trace, hdr);
  }

  @Override
  public void readTrace(long seqnoInDataset, float[] trace, int[] hdr) throws IOException {
    _ds.readTrace(seqnoInDataset, trace, hdr);
  }

  @Override
  public void readTraceHdr(long seqnoInDataset, int[] hdr) throws IOException {
    _ds.readTraceHdr(seqnoInDataset, hdr);
  }

  @Override
  public boolean writeFrame(SeisData seisData, long hypercubeKey, long volumeKey, long frameKey) throws IOException {
    return _ds.writeFrame(seisData, hypercubeKey, volumeKey, frameKey);
  }

  @Override
  public void close() throws IOException {
    _ds.close();
  }

  @Override
  public String getDescName() throws IOException {
    return _ds.getDescName();
  }

  @Override
  public String getFormatName() throws IOException {
    return _ds.getFormatName();
  }

  @Override
  public long getSize() {
    return _ds.getSize();
  }

  @Override
  public History readHistory() throws IOException {
    return _ds.readHistory();
  }

  @Override
  public void readTraceMap(int[][] map, long hypercubeKey) throws IOException {
    _ds.readTraceMap(map, hypercubeKey);
  }

  @Override
  public String[] getSortMaps() throws IOException {
    return _ds.getSortMaps();
  }

  @Deprecated
  @Override
  public DataCapsule readNextCapsule() throws IOException {
    throw new UnsupportedOperationException("readNextCapsule");
  }
}
