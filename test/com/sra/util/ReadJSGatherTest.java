/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/

package com.sra.util;


import java.io.IOException;

import com.lgc.prodesk.hdr.HdrCatalog;
import com.lgc.prowess.javaseis.smart.SmartFramework;
import com.lgc.prowess.seisdata.DataContextImpl;
import com.lgc.prowess.seisdata.SeismicDataset;
import junit.framework.TestCase;
import org.mockito.Mockito;

public class ReadJSGatherTest extends TestCase {
  private SeismicDataset _seismicDataset;
  private DataContextImpl _dataContext;
  private SmartFramework _smartFramework;
  private HdrCatalog _hdrCatalog;
  private long[] _axisSizes;
  public void setUp() throws Exception {
    _seismicDataset = Mockito.mock(SeismicDataset.class);

    _dataContext = Mockito.mock(DataContextImpl.class);
    _smartFramework = Mockito.mock(SmartFramework.class);
    _hdrCatalog = Mockito.mock(HdrCatalog.class);
    _axisSizes = new long[]{0l,1l,10l,7l};
    Mockito.when(_seismicDataset.getDataContext()).thenReturn(_dataContext);
    Mockito.when(_dataContext.getSmartFramework()).thenReturn(_smartFramework);
    Mockito.when(_smartFramework.getAxisLengths()).thenReturn(_axisSizes);
    Mockito.when( _dataContext.getHdrCatalog()).thenReturn(_hdrCatalog);

  }

  public void test1() throws IOException {
    try(ReadJSGather ignored = new ReadJSGather(_seismicDataset)) {

    }
    //Verify the seismic dataset gets closed
    Mockito.verify(_seismicDataset, Mockito.atLeastOnce()).close();

  }

}
