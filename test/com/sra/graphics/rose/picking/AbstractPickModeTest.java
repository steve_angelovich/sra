package com.sra.graphics.rose.picking;

import java.awt.Component;
import java.awt.event.MouseEvent;

import edu.mines.jtk.awt.ModeManager;
import edu.mines.jtk.mosaic.Tile;
import edu.mines.jtk.mosaic.Transcaler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Mockito test of common picking code.
 * @author Derek Parks
 */
public class AbstractPickModeTest {

  private AbstractPickMode _pickMode;
  @Before
  public void buildPickMode() {
    _pickMode = new AbstractPickMode(Mockito.mock(ModeManager.class), "test", "test", 0 ) {
        @Override
        protected void beginPanelSelect(MouseEvent e, Transcaler ts) {

        }

        @Override
        protected void endPanelSelect(MouseEvent e, Transcaler ts) {

        }

        @Override
        protected void duringPanelSelect(MouseEvent e, Transcaler ts) {

        }
      };
  }

  @Test
  public void testSetActive() {
    Component mockComp = Mockito.mock(Tile.class);
    _pickMode.setActive(mockComp, true);
    Mockito.verify(mockComp, Mockito.atLeastOnce()).addMouseListener(Mockito.anyObject());

    _pickMode.setActive(mockComp, false);
    Mockito.verify(mockComp, Mockito.atLeastOnce()).removeMouseListener(Mockito.anyObject());
  }
}
