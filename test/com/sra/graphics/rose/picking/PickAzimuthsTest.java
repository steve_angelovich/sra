/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;


import java.awt.Graphics2D;

import com.sra.graphics.rose.picking.PickAzimuths;
import com.sra.graphics.rose.picking.PickedOffsets;
import edu.mines.jtk.mosaic.Transcaler;
import junit.framework.TestCase;
import org.junit.Assert;

public class PickAzimuthsTest extends TestCase {

  private PickAzimuths _pa;

  @Override
  public void setUp() {
    _pa = new PickAzimuths(36, 10, new PickedOffsets.PaintHighlight() {
        @Override
        public void paint(Graphics2D g2d, Transcaler ts, double begin, double end) {}
      });
  }

  @Override
  public void tearDown() {
    for(int i=0; i < 36; i++) {
          System.out.println(i + " " + _pa.isPicked(i * 10));
    }
  }

  public void testClockWise() {

    _pa.pick(null, null, 20);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 0);
    _pa.pick(null, null, 350);
    _pa.pick(null, null, 340);

    Assert.assertTrue(_pa.isPicked(20));
    Assert.assertTrue(_pa.isPicked(10));
    Assert.assertTrue(_pa.isPicked(0));
    Assert.assertTrue(_pa.isPicked(350));
    Assert.assertTrue(_pa.isPicked(340));

    Assert.assertTrue(!_pa.isPicked(40));
    Assert.assertTrue(!_pa.isPicked(30));
  }

  public void testClockWiseRepeat() {

    _pa.pick(null, null, 20);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 0);
    _pa.pick(null, null, 350);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 340);

    Assert.assertTrue(_pa.isPicked(20));
    Assert.assertTrue(_pa.isPicked(10));
    Assert.assertTrue(_pa.isPicked(0));
    Assert.assertTrue(_pa.isPicked(350));
    Assert.assertTrue(_pa.isPicked(340));

    Assert.assertTrue(!_pa.isPicked(40));
    Assert.assertTrue(!_pa.isPicked(30));
  }


  public void testClockWiseBackward() {

    _pa.pick(null, null, 20);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 0);
    _pa.pick(null, null, 350);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 30);
    _pa.pick(null, null, 30);
    _pa.pick(null, null, 30);

    Assert.assertTrue(_pa.isPicked(20));
    Assert.assertTrue(_pa.isPicked(10));
    Assert.assertTrue(_pa.isPicked(0));
    Assert.assertTrue(_pa.isPicked(350));
    Assert.assertTrue(_pa.isPicked(340));
    Assert.assertTrue(_pa.isPicked(30));


    Assert.assertTrue(!_pa.isPicked(40));
    Assert.assertTrue(!_pa.isPicked(330));
    Assert.assertTrue(!_pa.isPicked(320));
    Assert.assertTrue(!_pa.isPicked(310));
  }


  public void testClockWiseSkip() {

    _pa.pick(null, null, 20);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 40);

    Assert.assertTrue(_pa.isPicked(40));
    Assert.assertTrue(_pa.isPicked(30));
    Assert.assertTrue(_pa.isPicked(30));
    Assert.assertTrue(_pa.isPicked(20));
    Assert.assertTrue(_pa.isPicked(10));
    Assert.assertTrue(_pa.isPicked(350));
    Assert.assertTrue(_pa.isPicked(340));


    Assert.assertTrue(!_pa.isPicked(50));
    Assert.assertTrue(!_pa.isPicked(330));
    Assert.assertTrue(!_pa.isPicked(320));
    Assert.assertTrue(!_pa.isPicked(310));
  }


  public void testAntiWise() {
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 350);
    _pa.pick(null, null, 0);
    _pa.pick(null, null, 10);

    Assert.assertTrue(_pa.isPicked(330));
    Assert.assertTrue(_pa.isPicked(340));
    Assert.assertTrue(_pa.isPicked(350));
    Assert.assertTrue(_pa.isPicked(0));
    Assert.assertTrue(_pa.isPicked(10));

    Assert.assertTrue(!_pa.isPicked(20));
    Assert.assertTrue(!_pa.isPicked(320));
  }


  public void testRepeat() {
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 330);

    Assert.assertTrue(_pa.isPicked(330));

    Assert.assertTrue(!_pa.isPicked(310));
    Assert.assertTrue(!_pa.isPicked(320));
    Assert.assertTrue(!_pa.isPicked(300));
    Assert.assertTrue(!_pa.isPicked(340));
    Assert.assertTrue(!_pa.isPicked(350));

  }

  public void testRepeatAntiWise() {
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 350);
    _pa.pick(null, null, 0);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 10);

    Assert.assertTrue(_pa.isPicked(330));
    Assert.assertTrue(_pa.isPicked(340));
    Assert.assertTrue(_pa.isPicked(350));
    Assert.assertTrue(_pa.isPicked(0));
    Assert.assertTrue(_pa.isPicked(10));

    Assert.assertTrue(!_pa.isPicked(20));
    Assert.assertTrue(!_pa.isPicked(320));
  }

  public void testAntiWiseBackward() {
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 340);
    _pa.pick(null, null, 350);
    _pa.pick(null, null, 0);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 10);
    _pa.pick(null, null, 330);
    _pa.pick(null, null, 320);

    Assert.assertTrue(_pa.isPicked(320));
    Assert.assertTrue(_pa.isPicked(330));
    Assert.assertTrue(_pa.isPicked(340));
    Assert.assertTrue(_pa.isPicked(350));
    Assert.assertTrue(_pa.isPicked(0));
    Assert.assertTrue(_pa.isPicked(10));
    Assert.assertTrue(!_pa.isPicked(20));
  }


}
