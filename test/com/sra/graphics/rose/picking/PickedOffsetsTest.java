/*******************************************************************************
 *    Copyright 2015 Steve Angelovich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *    limitations under the License. 
 *******************************************************************************/
package com.sra.graphics.rose.picking;

import java.awt.Graphics2D;

import com.sra.graphics.rose.picking.PickedOffsets;
import edu.mines.jtk.mosaic.Transcaler;
import org.junit.Assert;
import junit.framework.TestCase;

public class PickedOffsetsTest extends TestCase {
  PickedOffsets _po;
  public void test1() {
    _po.pick(null, null, 25);
    _po.pick(null, null, 75);

    Assert.assertTrue(_po.isPicked(25));
    Assert.assertTrue(_po.isPicked(50));
    Assert.assertTrue(_po.isPicked(75));

    Assert.assertTrue(!_po.isPicked(0));
    Assert.assertTrue(!_po.isPicked(100));
    _po.pick(null, null, 0);
    Assert.assertTrue(_po.isPicked(0));
  }

  @Override
    public void setUp() {
      _po = new PickedOffsets(10, 25, new PickedOffsets.PaintHighlight() {
            @Override
            public void paint(Graphics2D g2d, Transcaler ts, double begin, double end) {
            }
      });
    }

    @Override
    public void tearDown() {
      for(int i=0; i < 10; i++) {
            System.out.println(i + " " + _po.isPicked(i * 25));
      }
    }

}
