<?xml version="1.0"?>

<!-- ANT build file for SeisSpace Java code. -->

<project name="sra.tools" default="classes" basedir=".">
    <description>
        This is an example build.xml for packing a SeisSpace plugin.
    </description>

    <property environment="env"/>

    <!-- Default prowess.home to environment variable.  If necessary,
         override on command line with -Dprowess.home=/whatever. -->
    <property name="prowess.home" value="${env.PROWESS_HOME}"/>

    <!-- Default promax.home to environment variable.  If necessary,
         override on command line with -Dpromax.home=/whatever. -->
    <property name="promax.home" value="${env.PROMAX_HOME}"/>

    <!-- Load the plugin's properties from plugin.properties file. -->
    <property file="plugin.properties" prefix="plugin"/>

    <property name="plugin.version"
              value="${plugin.version.major}.${plugin.version.minor}.${plugin.version.subminor}"/>
    <property name="plugin"
              value="${plugin.product.name}_SSPI_${plugin.vendor.id}.${plugin.product.id}_${plugin.version}"/>

    <!-- Property file with some bitbucket details for publishing -->
    <property file="${user.home}/bb.properties" />

    <!-- Set some properties. -->
    <property name="productname" value="sra"/>
    <property name="sys" location="sys"/>
    <property name="doc" location="${sys}/doc"/>
    <property name="syslib" location="${sys}/lib"/>
    <property name="src" location="port"/>
    <property name="test" location="test"/>
    <property name="test.cls" location="${sys}/test"/>
    <property name="tmp" location="tmp"/>
    <property name="help" location="${src}/help/seisspace"/>
    <property name="etc" location="etc"/>
    <property name="report.dir" location="report"/>
    <property name="report.dir.xml" location="${report.dir}/xml"/>
    <property name="jarfile" location="${syslib}/${productname}.jar"/>
    <property name="tarfile" location="${plugin}.tar.gz"/>


    <path id="classpath.compile">
        <pathelement location="${prowess.home}/build/cls"/>
        <pathelement location="${prowess.home}/sys/com_lgc_seisspace.jar"/>
        <fileset dir="${prowess.home}/sys/lib" includes="**/*.jar"/>
        <fileset dir="nodist/lib" includes="**/*.jar"/>
    </path>

    <path id="classpath.test">
        <pathelement location="${test.cls}"/>
        <pathelement location="${jarfile}"/>
        <path refid="classpath.compile"/>
    </path>

    <target name="all" depends="info, jar, doc, unittests, plugin"
            description="Runs info jar doc unittests plugin"/>

    <target name="init">
        <mkdir dir="${help}"/>
        <mkdir dir="${sys}"/>
        <mkdir dir="${syslib}"/>
        <mkdir dir="${test}"/>
        <mkdir dir="${test.cls}"/>
    </target>


    <macrodef name="assertDirAvailable">
        <attribute name="dir" />
        <sequential>
            <fail message="The directory '@{dir}' was not found">
                <condition>
                    <not>
                        <available file="@{dir}" type="dir" />
                    </not>
                </condition>
            </fail>
        </sequential>
    </macrodef>

    <target name="classes" depends="init"
            description="Compile all Java sources.">
        <assertDirAvailable dir="${prowess.home}" />
        <javac srcdir="${src}"
               destdir="${sys}"
               debug="on"
               source="1.7"
               fork="yes"
               includeantruntime="false"
               deprecation="on"
               classpathref="classpath.compile">
            <compilerarg value="-Xlint:all,-serial"/>
        </javac>

        <copy todir="${sys}">
            <fileset dir="${src}"
                     includes="**/*.xml
                         **/*.properties
                         **/*.txt
                         **/*.ico
                         **/*.png"/>
        </copy>

    </target>

    <target name="classes.tests" depends="classes"
            description="Compile all Java test sources.">
        <javac srcdir="${test}"
               destdir="${test.cls}"
               debug="on"
               source="1.7"
               fork="yes"
               includeantruntime="false"
               deprecation="on"
               classpathref="classpath.test">
            <compilerarg value="-Xlint:all,-serial"/>
        </javac>

        <copy todir="${test}">
            <fileset dir="${test.cls}"
                     includes="**/*.xml
                         **/*.properties
                         **/*.txt
                         **/*.ico
                         **/*.png"/>
        </copy>

    </target>

    <target name="clean"
            description="Delete build products.">
        <delete dir="${help}" quiet="true"/>
        <delete dir="${sys}" quiet="true"/>
        <delete dir="${report.dir}" quiet="true"/>
        <delete dir="${test.cls}" quiet="true"/>
        <delete file="${jarfile}" quiet="true"/>
        <delete file="${tarfile}" quiet="true"/>
        <delete file="${tarfile}.MD5" quiet="true"/>
    </target>

    <target name="jar" depends="classes"
            description="Create a jar of the classes.">
        <jar destfile="${jarfile}" compress="true" basedir="${sys}"
             includes="com/**"/>
    </target>

    <target name="plugin" depends="jar, help"
            description="Creates this plugin's .tar.gz archive for deployment.">
        <delete file="${tarfile}" quiet="true"/>

        <tar destfile="${tarfile}" compression="gzip" longfile="posix">
            <tarfileset prefix="${plugin}" dir="${basedir}"
                        includes="plugin.properties, sys/**, etc/**, port/help/**"
                        excludes="sys/com/**, sys/doc/**"/>
        </tar>

        <!-- Create a MD5 sum of the results. -->
        <checksum file="${tarfile}" format="MD5SUM"/>
    </target>

    <target name="help" depends="init"
            description="Build port/help/seisspace.">
        <copy todir="${help}">
            <fileset dir="DOCS/pdfs" includes="*.pdf"/>
        </copy>
    </target>

    <target name="unittests" depends="jar,classes.tests">
        <mkdir dir="${report.dir}"/>
        <echo message="Execute Tests in Batch"/>
        <junit fork="yes" printsummary="yes" haltonfailure="no">

            <classpath>
                <pathelement location="${test.cls}"/>
                <path refid="classpath.test"/>
            </classpath>
            <env key="LD_LIBRARY_PATH" path="${prowess.home}/sys/linux64/lib:${promax.home}/linux64/lib"/>
            <env key="LD_PRELOAD" path="${env.JAVA_HOME}/jre/lib/amd64/server/libjsig.so"/>
            <jvmarg value="-ea"/>
            <jvmarg value="-XX:NewRatio=4"/>
            <batchtest fork="yes" todir="${report.dir}">
                <fileset dir="${test}"> <include name="**/*Test*.java" />
                </fileset>
            </batchtest>
            <formatter type="xml"/>
        </junit>

        <echo message="Format the tests report"/>
        <junitreport todir="${report.dir}">
            <fileset dir="${report.dir}">
                <include name="TEST-*.xml"/>
            </fileset>
            <report todir="${report.dir}"/>
        </junitreport>

        <echo message="Unit test report available at ${report.dir}${file.separator}index.html"/>
        <fail message="Test failed." if="test.failed"/>

    </target>

    <target name="doc" depends="init" description="Create the Javadocs.">
        <delete dir="${doc}"/>
        <mkdir dir="${doc}"/>
        <javadoc sourcepath="${src}"
                 classpathref="classpath.compile"
                 packagenames="com.*"
                 excludepackagenames="com.**.test"
                 defaultexcludes="yes"
                 destdir="${doc}"
                 author="true"
                 version="true"
                 use="true"
                 access="protected"
                 additionalparam="-breakiterator">
        </javadoc>
    </target>

    <target name="info" description="Print out selected properties.">
        <!-- Convert paths to properties so we can print them. -->
        <property name="classpath.compile.property" refid="classpath.compile"/>
        <property name="classpath.test.property" refid="classpath.test"/>

        <echo>
            PROWESS_HOME = ${env.PROWESS_HOME}
            prowess.home = ${prowess.home}
            PROMAX_HOME = ${env.PROMAX_HOME}
            promax.home = ${promax.home}
            pmss.release = ${pmss.release}
            JAVA_HOME = ${env.JAVA_HOME}
            JAVACMD = ${env.JAVACMD}
            ld.library.path = ${ld.library.path}
            LD_LIBRARY_PATH = ${env.LD_LIBRARY_PATH}
            classpath.compile = ${classpath.compile.property}
            classpath.test = ${classpath.test.property}
        </echo>

        <echo>
            ***********************************************************************
            plugin = ${plugin}
            plugin.product.name = ${plugin.product.name}
            plugin.product.id = ${plugin.product.id}
            plugin.vendor.name = ${plugin.vendor.name}
            plugin.vendor.id = ${plugin.vendor.id}
            plugin.version.major = ${plugin.version.major}
            plugin.version.minor = ${plugin.version.minor}
            plugin.version.subminor = ${plugin.version.subminor}
            plugin.version.date = ${plugin.version.date}
            plugin.type = ${plugin.type}
            plugin.description = ${plugin.description}
            ***********************************************************************
        </echo>
    </target>

   <target name="publish" depends="plugin" description="Publish to bitbucket">
     <echo message="Publishing ${tarfile}.MD5" />
     <exec executable="nodist/bin/bbpost" >
       <arg value="-user" />
       <arg value="${bb.username}" />
       <arg value="-pass" />
       <arg value="${bb.password}" />
       <arg value="-proj" />
       <arg value="${bb.proj}" />
       <arg value="-post" />
       <arg value="${tarfile}.MD5" />
     </exec>
     <echo message="Publishing ${tarfile}" />
     <exec executable="nodist/bin/bbpost" >
       <arg value="-user" />
       <arg value="${bb.username}" />
       <arg value="-pass" />
       <arg value="${bb.password}" />
       <arg value="-proj" />
       <arg value="${bb.proj}" />
       <arg value="-post" />
       <arg value="${tarfile}" />
     </exec>
   </target>

</project>
